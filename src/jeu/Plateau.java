package jeu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;
import static jeu.EtatPlateau.tEtatDisque;
import static jeu.EtatPlateau.dimPlateau;
/**
 *
 * @author rsmon
 */
public class Plateau  extends JPanel{
  
    private    Ellipse2D[][]   tDisque           = new Ellipse2D.Double[dimPlateau][dimPlateau];
     
    private    Color[]         couleursDisque    = 
    
               { Color.WHITE  , Color.ORANGE    , Color.BLUE,
                 Color.GREEN  , Color.MAGENTA   , Color.RED,
                 Color.YELLOW , Color.PINK      , Color.CYAN,
                 Color.BLACK  , Color.DARK_GRAY , Color.GRAY
               };
     
    private    Double          rayonDisque       = 8.0; 
    private    Color           couleurCadre      = Color.YELLOW;
    
    private    Rectangle2D     cadre             = new Rectangle2D.Double(10, 10, 640,640);
          
    public Plateau() {
       
      this.setBackground(Color.LIGHT_GRAY); 
      creerDisques(); 
    }
    
    final void creerDisques() {
        
      for( int i=0; i<dimPlateau; i++ ) for( int j=0; j<dimPlateau; j++ ) {
                
        tDisque[i][j]= new Ellipse2D.Double
                       (                      
                         10+rayonDisque*i,
                         10+rayonDisque*j,
                         rayonDisque, 
                         rayonDisque
                       );               
      }
    }
     
    @Override
    public void paintComponent(Graphics g) {
        
      super.paintComponent(g); Graphics2D g2d = (Graphics2D)g;
     
      g.setColor(couleurCadre);g2d.draw(cadre);
      
      for( int i=0; i<dimPlateau; i++ ) for( int j=0; j<dimPlateau; j++) { 
         
         g.setColor( couleursDisque[ tEtatDisque[i][j]%1000 ] ); 
         g2d.fill(tDisque[i][j]); 
      }  
    } 
    
    public void rafraichir(){ repaint();}
    
    public Ellipse2D[][] gettDisque() { return tDisque; }
}
