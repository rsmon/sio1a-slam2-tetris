
package jeu;

/**
 *
 * @author rsmon
 */
public class CodeTouches {
    
   public static final int TOUCHE_F=70;  // Afficher une suite de figures ( pour test)
   public static final int TOUCHE_R=82;  // Rotation  sens anti-horaire
   public static final int TOUCHE_L=76;  // Rotation sens horaire
   public static final int TOUCHE_S=83;  // Symetrie axiale  diagonale /
   public static final int TOUCHE_D=68;  // Symetrie axiale  diagonale \
   public static final int TOUCHE_H=72;  // Symetrie axiale mediane  --
   public static final int TOUCHE_V=86;  // Symetrie axiale mediane  |
   public static final int TOUCHE_C=67;  // Symetrie axiale entrale
   public static final int FLECHE_D=39;  // translation vers la droite 
   public static final int FLECHE_G=37;  // translation vers la  gauche
   public static final int FLECHE_H=40;  // translationvers le haut
   public static final int FLECHE_B=38;  // translationvers le bas
   public static final int BACK_SP = 8;  // Viderle plateau
}
