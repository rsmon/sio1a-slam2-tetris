
package jeu;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static jeu.Automate.changerDeFigureActive;

/**
 *
 * @author rsmon
 */
public class ListenerSouris extends MouseAdapter{

     @Override
     public void mouseClicked(MouseEvent me) {
        
        super.mouseClicked(me);
        
        int xSouris=me.getX();
        int ySouris=me.getY();

        changerDeFigureActive(xSouris, ySouris);           
            
    }

}
