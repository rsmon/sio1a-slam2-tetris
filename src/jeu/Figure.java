
package jeu;

import static jeu.EtatPlateau.tEtatDisque;
import static jeu.LesFigures.getFigure;

/**
 *
 * @author rsmon
 */
public class Figure {
    
     private   int           dimFig;
    
     private   int[][]       etatPoints;
     private   int[][]       etatPointsPrecedent;
     
     private   int           x;
     private   int           y;

     //<editor-fold defaultstate="collapsed" desc="Constructeur">
     
     public Figure( int[][]  pEtatPoints) {
         
         this.dimFig              = pEtatPoints.length;
         this.etatPoints          = new int[dimFig][dimFig];
         this.etatPointsPrecedent = new int[dimFig][dimFig];
         
         for ( int i=0;i<dimFig;i++) System.arraycopy(pEtatPoints[i], 0, this.etatPoints[i], 0, dimFig);
     }
     
     //</editor-fold>
     
     //<editor-fold defaultstate="collapsed" desc="Méthodes  Isométries">
     
     public void rotation( boolean droite){  // effectue un quart de tour, droite=   true: sens horaire, false: antihoraire 
         
         int nx, ny;
         
         sauveEtatPred();
         
         for(int i=0; i<dimFig;i++ )for(int j=0;j<dimFig;j++ ){
             
             if(droite) {nx= -j+dimFig-1; ny= i;}else{ nx= j; ny=-i+dimFig-1;}
             
             etatPoints[nx][ny]=(etatPointsPrecedent[i][j]);
         }
     }
     
     public void symMiroirDiag( boolean diagMonte){  // diagMonte= true: /  , false: \
         
         int nx, ny;
         
         sauveEtatPred();
         
         for(int i=0; i<dimFig;i++ )for(int j=0;j<dimFig;j++ ){
             
             if (diagMonte) {nx= j; ny=i;} else {nx=-j+dimFig-1;ny=-i+dimFig-1;}
             
             etatPoints[nx][ny]=(etatPointsPrecedent[i][j]);
         }
     }
     
     public void symMiroirMed( boolean axeHoriz){  // axeHoriz=       true:   --  , false  |   
         
         int nx, ny;
         
         sauveEtatPred();
         
         for(int i=0; i<dimFig;i++ )for(int j=0;j<dimFig;j++ ){
             
             if (axeHoriz) {nx= -i+dimFig-1; ny=j;} else {nx=i;ny=-j+dimFig-1;}
             
             etatPoints[nx][ny]=(etatPointsPrecedent[i][j]);
             
         }
     }
     
     public void symCentrale(){
         
         int nx, ny;
         
         sauveEtatPred();
         
         for(int i=0; i<dimFig;i++ )for(int j=0;j<dimFig;j++ ){
             
             nx= -i+dimFig-1;ny=-j+dimFig-1;
             
             etatPoints[nx][ny]=(etatPointsPrecedent[i][j]);
             
         }
     }
      
     //</editor-fold>
     
     //<editor-fold defaultstate="collapsed" desc="Methode de sauvegarde d'état">
    
     private void sauveEtatPred(){
         
         for(int i=0; i<dimFig;i++ )System.arraycopy(etatPoints[i], 0, etatPointsPrecedent[i], 0, dimFig);
     }
     
     //</editor-fold>
    
     //<editor-fold defaultstate="collapsed" desc="Getters et Setters"> 
    
 
    public int getDim() {
        return dimFig;
    }
    
    public void setDim(int dim) {
        this.dimFig = dim;
    }
    public int[][] getEtatPoints() {
        return etatPoints;
    }

    public void setEtatPoints(int[][] etatPoints) {
        this.etatPoints = etatPoints;
    }
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    //</editor-fold>

    
}