
package jeu;

/**
 *
 * @author rsmon
 */
public class LesFigures {
     
    private static int [][] valFigure1 = 
                          {  
                           { 4, 1,1,2,3,3,5},
                           { 1,11,1,2,3,6,3},
                           { 1, 1,1,2,3,3,3},
                           { 2, 2,2,0,2,2,2},
                           { 5, 5,5,2,4,0,0},
                           { 5, 7,5,2,0,0,0},
                           { 3, 5,5,2,0,0,0}
                          };
    
    private static int [][] valFigure2= 
                          { 
                           { 0,0,0,2,0,0,0},
                           { 0,0,0,2,0,0,0},
                           { 0,0,1,2,3,0,0},
                           { 2,2,2,0,2,2,2},
                           { 0,0,5,2,4,0,0},
                           { 0,0,0,2,0,0,0},
                           { 0,0,0,2,0,0,0}
                          };
    
    private static int [][] valFigure3 = 
                          {
                           { 0, 4, 4},
                           { 4,10, 4},
                           { 4, 4, 1}
                          };
    
    private static int [][] valFigure4 = 
                          {
                           { 0,3,3},
                           { 3,6,3},
                           { 3,3,5}
                          };
    
    private static int [][] valFigure5 = 
                          {
                           { 0, 5, 5},
                           { 5, 7, 5},
                           { 5, 5, 3}
                          };
    
    
    private static int [][] valFigure6 = 
                          {
                           { 0,  1, 1},
                           { 1, 11, 1},
                           { 1,  1, 4}
                          };
     
    private static int [][] valFigure10=
                          {
                           { 6,2,5,1,3,6},
                           { 2,6,5,1,6,3},
                           { 5,5,6,6,1,1},
                           { 3,3,6,6,2,2},
                           { 1,6,3,2,6,5},
                           { 6,1,3,2,5,6}            
                          };
    
    private static Figure[]   tabFigures= new Figure[12]; 
       
    //</editor-fold>
   
    static{
    
       tabFigures[ 1] = new Figure(valFigure1);
       tabFigures[ 2] = new Figure(valFigure2); 
       tabFigures[ 3] = new Figure(valFigure3);
       tabFigures[ 4] = new Figure(valFigure4);
       tabFigures[ 5] = new Figure(valFigure5);
       tabFigures[ 6] = new Figure(valFigure6);
       tabFigures[10] = new Figure(valFigure10);   
    }
        
    public static Figure  getFigure(int noFig){ return tabFigures[noFig]; }
    
}
