
package jeu;

import static jeu.LesFigures.getFigure;
import static jeu.EtatPlateau.*;

/**
 *
 * @author rsmon
 */

public final class Automate {
   
   private  static   Plateau   plateau;
   private  static   int       xDepartFigUn=10, yDepartFigUn=10;   
            static   int       noFigActive=0; 
    
   public static void  afficheFigure(int noFig, int x, int y) {  // Afficher la figure ou on le souhaite en x,y
      
       if (noFig==0)return;
     
       Figure figure=getFigure(noFig);  
       int dimFig= figure.getDim();
     
       for(int i=0; i<dimFig;i++ )for(int j=0;j<dimFig;j++ ){
      
         tEtatDisque[ x + i][ y +j ] = figure.getEtatPoints()[i][j]+1000*noFig;
       } 
       figure.setX(x); figure.setY(y);
       plateau.rafraichir(); 
    }
    
   public static void  afficheFigure(int noFig) {  // Afficher la figure ou elle se trouve ( après une translation ou une isometrie)
      
      if (noFig==0)return; 
      
      Figure figure=getFigure(noFig);  
      int dimFig= figure.getDim();
     
      for(int i=0; i<dimFig;i++ )for(int j=0;j<dimFig;j++ ){
      
         tEtatDisque[ figure.getX() + i][ figure.getY() +j ] = figure.getEtatPoints()[i][j]+1000*noFig;
      } 
      
      plateau.rafraichir(); 
    }
    
   public static void  translaterFigureHoriz(int noFig, int dec){
      
      if (noFig==0)return;
     
      Figure figure=getFigure(noFig);      
      effacerFigure(noFig);
    
      figure.setX(figure.getX()+dec);
     
      afficheFigure(noFig);
    }
     
   public static void  translaterFigureVerti(int noFig, int dec){
      
      if (noFig==0)return;
     
      Figure figure=getFigure(noFig);   
      effacerFigure(noFig);   
      figure.setY(figure.getY()+dec);
      afficheFigure(noFig);
    }
  
   public static void  symCentrale(int noFig){
      
      if (noFig==0)return;
        
      getFigure(noFig).symCentrale();
      afficheFigure(noFig);
    }
     
   public static void  rotation( int noFig,boolean droite){
      
      if (noFig==0)return;
      
      getFigure(noFig).rotation(droite);
      afficheFigure(noFig);
    }
       
   public static void  symMiroirDiag( int noFig, boolean diagMonte){
      
      if (noFig==0)return;
    
      getFigure(noFig).symMiroirDiag(diagMonte);
      afficheFigure(noFig);
    }
    
   public static  void symMiroirMed( int noFig, boolean axeHoriz){
      
      if (noFig==0)return;
   
      getFigure(noFig).symMiroirMed(axeHoriz);
    
      afficheFigure(noFig);
    }
    
   public static void  initFigures(){
       effacerPlateau();
       for (int i=1;i<7;i++) afficheFigure( i,xDepartFigUn+10*i , yDepartFigUn+10*i );
    }    
       
   public static  void effacerFigure(int noFig){
      
       if (noFig==0)return;
       
       Figure figure=getFigure(noFig); 
        
       int dimFig=figure.getDim();
        
       for(int i=0; i<dimFig;i++ )for(int j=0;j<dimFig;j++ ){
    
           tEtatDisque[figure.getX() +i ][figure.getY()+ j]= 0;
       }
    }
    
   public static void  effacerPlateau(){
    
         for(int i=0; i<dimPlateau;i++ )for(int j=0;j<dimPlateau;j++ ){ tEtatDisque[+i ][ j]= 0;}
         noFigActive=0;
         plateau.rafraichir();
    }
   
   public  static void changerDeFigureActive(int xSouris, int ySouris) {
        
       int xDisque=-1,yDisque=-1;
        
        boolean clickDansUnDisque=false;
        
        for (int i=0;i<dimPlateau;i++)for(int j=0;j<dimPlateau;j++){
            
           if ( plateau.gettDisque()[i][j].contains(xSouris,ySouris) ){ 
               
               clickDansUnDisque=true; 
               xDisque=i; yDisque=j; 
               break;
           }
        }  
        
        if (clickDansUnDisque){ 
            
            int etat=tEtatDisque[xDisque][yDisque];
            
            int couleur=etat%1000;
            int noFig=etat/1000;
            
            if (noFig!=0) noFigActive=noFig;
           
        }
    }
       
   public  static int  getNoFigActive() { return noFigActive; }
   
   public  static void setPlateau(Plateau plateau) { Automate.plateau = plateau; }
}
