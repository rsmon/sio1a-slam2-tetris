
package jeu;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import static jeu.CodeTouches.*;
import static jeu.Automate.*;
/**
 *
 * @author rsmon
 */

public class ListenerTouches extends KeyAdapter {
     
    @Override
    public void keyPressed(KeyEvent ke) {
          
      switch (ke.getKeyCode()) {
             
          case FLECHE_G : translaterFigureHoriz(noFigActive,-1) ; break; 
          case FLECHE_B : translaterFigureVerti(noFigActive,-1) ; break;  
          case FLECHE_D : translaterFigureHoriz(noFigActive,+1) ; break; 
          case FLECHE_H : translaterFigureVerti(noFigActive,+1) ; break;   
          case TOUCHE_C : symCentrale(noFigActive)              ; break;
          case TOUCHE_D : symMiroirDiag(noFigActive,true)       ; break;   
          case TOUCHE_F : initFigures()                         ; break;
          case TOUCHE_H : symMiroirMed(noFigActive,false)       ; break;
          case TOUCHE_L : rotation(noFigActive,true)            ; break;
          case TOUCHE_R : rotation(noFigActive,false)           ; break;
          case TOUCHE_S : symMiroirDiag(noFigActive,false)      ; break;
          case TOUCHE_V : symMiroirMed(noFigActive,true)        ; break;    
          case BACK_SP  : effacerPlateau();                     ; break;
      } 
    } 
}
